﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotoFoot
{

    /// <summary>
    /// Logique d'interaction pour GestionButeurs.xaml
    /// </summary>

    public partial class GestionButeurs : Window
    {
        private List<Buteur> buteur1;
        private List<Buteur> buteur2;
        private MainWindow main;
        public GestionButeurs(MainWindow main, List<Buteur> buteur1, List<Buteur> buteur2)
        {
            this.main = main;
            InitializeComponent();
            this.buteur1 = buteur1;
            this.buteur2 = buteur2;
            dg1.ItemsSource = buteur1;
            dg2.ItemsSource = buteur2;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            main.misAJourButeurs(buteur1,buteur2);
            Close();
        }
    }
}
