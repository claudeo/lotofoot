﻿/*
 * Created by SharpDevelop.
 * User: parent.claude
 * Date: 08/24/2015
 * Time: 08:40
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace LotoFoot
{
	/// <summary>
	/// Interaction logic for SelectionJoueur.xaml
	/// </summary>

	public partial class SelectionJoueur : Window
	{
		private bool estSupressionJoueur;
		private List<Joueur> joueurs;
		private ClasseDonnees donnees;
		private List<string> nomJoueurs;
		public SelectionJoueur(ClasseDonnees donnees, bool estSuppression)
		{
			InitializeComponent();
			this.joueurs=donnees.joueurs;
			this.estSupressionJoueur=estSuppression;
			this.donnees=donnees;
			nomJoueurs=new List<string>();
			for(int i=0;i<joueurs.Count;i++)
			{
				nomJoueurs.Add(joueurs[i].Nom);
			}
			comboJoueur.ItemsSource=nomJoueurs;
		}
		
		
		void valider_Click(object sender, RoutedEventArgs e)
		{
			if(comboJoueur.SelectedIndex>=0)
			{
				if(estSupressionJoueur)
				{
					joueurs.RemoveAt(comboJoueur.SelectedIndex);
				}
				else
				{
					Window2 w=new Window2(joueurs[comboJoueur.SelectedIndex],donnees);
					this.Visibility = Visibility.Hidden;
					w.ShowDialog();
				}
				Close();
			}
		}
	}
}