﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotoFoot
{
	[Serializable]
	public class Joueur
	{
		private string nom;
		private int score;
		private List<int> scorePronoParJournee;
		private List<Grille> pronostiques;
		private List<string> podium;
		private List<string> relegables;
		private List<int> pointButeurParJournee;
		private Grille grilleActuelle;
		private bool aMise;
		private bool aPayer;
		private List<List<string>> buteurs;
		private List<int> pointBonusSeul;
		private List<List<bool>> matchPronoSeul;
		List<string> buteursActuels;
		private bool aChangeButeur;
		private bool aPronostique;
		private bool aBonus;
		private bool aMalus;
		public bool AChangeButeur
		{
			get
			{
				return aChangeButeur;
			}
			set
			{
				aChangeButeur=value;
			}
		}
		public List<string> ButeursActuels
		{
			get
			{
				return buteursActuels;
			}
		}
		
		public int Rank
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string emailPerso
		{
			get;
			set;
		}

		public int Score
		{
			get
			{
				return score;
			}
		}

		public List<int> ScorePronoParJournee
		{
			get
			{
				return scorePronoParJournee;
			}
		}

		public List<int> PointButeurParJournee
		{
			get
			{
				return pointButeurParJournee;
			}
		}

		public bool APronostique
		{
			get
			{
				return aPronostique;
			}
		}

		public Grille GrilleActuelle
		{
			get
			{
				return grilleActuelle;
			}

			set
			{
				grilleActuelle = value;
			}
		}

		public List<Grille> PronostiquesPrecedents
		{
			get
			{
				return pronostiques;
			}
		}
        public List<int> PointBonusSeul
        {
            get
            {
                return pointBonusSeul;
            }
        }
        public List<List<bool>> MatchPronoSeul
        {
            get
            {
                return matchPronoSeul;
            }
        }
		public Joueur(ClasseCreationJoueur j)
		{
			nom = j.nom;
			scorePronoParJournee = new List<int>();
			pointButeurParJournee = new List<int>();
			pronostiques = new List<Grille>();
			buteurs = new List<List<string>>();
			matchPronoSeul = new List<List<bool>>();
			pointBonusSeul = new List<int>();
			buteursActuels = new List<string>();
			podium = new List<string>();
			relegables = new List<string>();
			buteursActuels.Add(j.buteur1);
			buteursActuels.Add(j.buteur2);
			buteursActuels.Add(j.buteur3);
			email = j.Email;
			emailPerso = j.EmailPerso;
			aMise = j.AMise;
			aPayer = j.APaye;
			podium.Add(j.Premier);
			podium.Add(j.Second);
			podium.Add(j.Troisieme);
			relegables.Add(j.Antepenultiemme);
			relegables.Add(j.AvantDernier);
			relegables.Add(j.Dernier);
		}

		public void appliquerJoueur(ClasseCreationJoueur j)
		{
			nom = j.nom;
			buteurs = new List<List<string>>();
			buteursActuels = new List<string>();
			podium = new List<string>();
			relegables = new List<string>();
			buteursActuels.Add(j.buteur1);
			buteursActuels.Add(j.buteur2);
			buteursActuels.Add(j.buteur3);
			email = j.Email;
			emailPerso = j.EmailPerso;
			aMise = j.AMise;
			aPayer = j.APaye;
			podium.Add(j.Premier);
			podium.Add(j.Second);
			podium.Add(j.Troisieme);
			relegables.Add(j.Antepenultiemme);
			relegables.Add(j.AvantDernier);
			relegables.Add(j.Dernier);
		}

		public string Nom { get { return nom; } }

		internal void validerGrille()
		{
			aPronostique = true;
			foreach(Match m in grilleActuelle.Matchs)
			{
				aPronostique = aPronostique && m.Pronostique != Prono.Rien;
			}
		}

		internal bool calculResultatGrille(Grille grilleRef, List<bool> estSeul)
		{
			if(pointBonusSeul == null)
			{
				pointBonusSeul = new List<int>();
			}

			if(matchPronoSeul == null)
			{
				matchPronoSeul = new List<List<bool>>();
			}

			List<bool> temp = new List<bool>();
			int nb = 0;
			int pointSeul = 0;
			bool aRepondu=false;
			for (int i = 0; i < grilleActuelle.Matchs.Count; i++)
			{
				temp.Add(false);
				if(grilleActuelle.Matchs[i].Pronostique != Prono.Rien)
				{
                    aRepondu = true;
				}
				
				if (grilleActuelle.Matchs[i].Pronostique == grilleRef.Matchs[i].Pronostique)
				{
					if(estSeul[i])
					{
						temp[i] = true;
						pointSeul++;
					}

					nb++;
				}
			}
            matchPronoSeul.Add(temp);
			scorePronoParJournee.Add(nb);
			score += nb;
			pointBonusSeul.Add(pointSeul);
			score+=pointSeul;
            pronostiques.Add(grilleActuelle);
            buteurs.Add(buteursActuels);
            grilleActuelle = null;
            aPronostique=false;
            return aRepondu;
        }

        internal void appliquerPointButeur(Dictionary<string, Buteur> buteurChapeau1, Dictionary<string, Buteur> buteurChapeau2, Dictionary<string, Buteur> buteurChapeau3)
        {
            int pointButeur = 0;
            if (buteursActuels[0] != "")
            {
                pointButeur += buteurChapeau1[buteursActuels[0]].ButGrilleActuelle;
                pointButeur += buteurChapeau2[buteursActuels[1]].ButGrilleActuelle;
                pointButeur += buteurChapeau3[buteursActuels[2]].ButGrilleActuelle;
            }
            score += pointButeur;
            pointButeurParJournee.Add(pointButeur);
        }

        internal void verifierBunusMalus(bool minChange, int minParisButteurs, bool maxChange, int maxParisButteurs)
        {
            int indice=ScorePronoParJournee.Count - 1;
            int pointTotalGrille=scorePronoParJournee[indice]+ pointBonusSeul[indice]+pointButeurParJournee[indice];
            aBonus = aBonus && !maxChange || pointTotalGrille == maxParisButteurs;
            aMalus = aMalus && !minChange || pointTotalGrille == minParisButteurs;
        }

        internal void appliquerScoreGrilleNonRendue(int p)
        {
            ScorePronoParJournee[ScorePronoParJournee.Count - 1] = p;
            score += p;
        }
    }
}
