﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Drawing;

namespace LotoFoot
{
    class ExportExcel
    {
        public static Color bleu = Color.FromArgb(55, 96, 145);
        public static Color noir = Color.FromArgb(0, 0, 0);
        public static void creerExcel(ClasseDonnees donnees, MainWindow m, bool estRemplie, string path)
        {

            Application app = new Application();
            app.ScreenUpdating=false;
            if(path=="")
            {
            	app.Visible = true;
            	app.ScreenUpdating=false;
            	
            }
            
            Workbook wb = app.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            app.Calculation= XlCalculation.xlCalculationManual;
            Worksheet wsRecap = (Worksheet) wb.Sheets.Add();
            Worksheet wsScores = (Worksheet) wb.Sheets.Add();

            wsScores.Name="Scores Cummulés";
            wsScores.Visible= XlSheetVisibility.xlSheetHidden;
            wsRecap.Name = "Recapitulatif Resultats";
            int decx = 0;
            List<int> pointParJoueur= new List<int>();
            List<int> pointButeurParJoueur= new List<int>();
            List<int> pointPriseRisqueParJoueur= new List<int>();
            List<int> pointPronoPurJoueur= new List<int>();
            List<int> rangParJoueur= new List<int>();
            for(int i=0;i<donnees.grillesPrecedente.Count;i++)
            {
            	for(int j=0;j<donnees.joueurs.Count;j++)
            	{
            		if(i==0)
            		{
            			pointParJoueur.Add(0);
            			pointButeurParJoueur.Add(0);
            			pointPriseRisqueParJoueur.Add(0);
            			pointPronoPurJoueur.Add(0);
            			rangParJoueur.Add(0);
            		}
            		pointButeurParJoueur[j]+=donnees.joueurs[j].PointButeurParJournee[i];
            		pointPronoPurJoueur[j]+=donnees.joueurs[j].ScorePronoParJournee[i];
            		pointPriseRisqueParJoueur[j]+=donnees.joueurs[j].PointBonusSeul[i];
            		pointParJoueur[j]=pointButeurParJoueur[j]+pointPronoPurJoueur[j]+pointPriseRisqueParJoueur[j];
            	}
            	
            	affichageResultat(donnees,wsRecap,decx,i);
            	rangParJoueur= affScoreRankIntermediaire(pointParJoueur,wsRecap,decx,donnees.grillesPrecedente[i].Matchs.Count);
            	afficherScoresCummules(wsScores,i,pointParJoueur,pointPronoPurJoueur,pointButeurParJoueur,pointPriseRisqueParJoueur, rangParJoueur);
            	decx+=donnees.grillesPrecedente[i].Matchs.Count+6 +4;
            }
            creerGraphiques(wb,donnees);
            Worksheet wsClassementButeur = (Worksheet) wb.Sheets.Add();
            wsClassementButeur.Name="Classement des Buteurs";
            afficherClassementButeurs(donnees.Buteurs,wsClassementButeur);
            Worksheet wsButButeur = (Worksheet) wb.Sheets.Add();
            wsButButeur.Name= "Buts par Buteur";
            afficherButeurs(donnees.Buteurs,wsButButeur);
            Worksheet wsClassement = (Worksheet) wb.Sheets.Add();
            wsClassement.Name="Classement";
            affClassement(wsClassement,donnees.joueurs);
            
            Worksheet ws = (Worksheet) wb.Sheets.Add();
            ws.Name = "Derniers Résultat";

            int indiceMatch = donnees.grillesPrecedente.Count - 1;               
            if(!estRemplie)
            {
            	indiceMatch = donnees.grillesPrecedente.Count;
            }
            affichageResultat(donnees,ws,0,indiceMatch);
			app.Calculation= XlCalculation.xlCalculationAutomatic;
            if(path!="")
            {
            	wb.SaveAs(path);
            	wb.Close();
            	app.Quit();
            }
            else
            {
            	app.ScreenUpdating=true;
            }

        }
        public static void afficherScoresCummules(Worksheet ws, int decx, List<int> points,List<int> pointsprono,List<int> pointsbuteur,List<int> pointsseul, List<int> rang)
        {
        	for(int i=0;i<points.Count;i++)
        	{
        		((Range)ws.Cells[1+decx,1+(i*5)]).Value=points[i];
        		((Range)ws.Cells[1+decx,2+(i*5)]).Value=pointsprono[i];
        		((Range)ws.Cells[1+decx,3+(i*5)]).Value=pointsbuteur[i];
        		((Range)ws.Cells[1+decx,4+(i*5)]).Value=pointsseul[i];
        		((Range)ws.Cells[1+decx,5+(i*5)]).Value=rang[i];
        	}
        }
        public static void affClassement(Worksheet ws,List<Joueur> joueurs)
        {
        	
        	int ligneDep=2;
        	int k=ligneDep+1;
        	((Range)ws.Cells[ligneDep,3]).Value="Nom";
        	((Range)ws.Cells[ligneDep,2]).Value="Rang";
        	((Range)ws.Cells[ligneDep,4]).Value="Score";
        	for(int i=1;i<=joueurs.Count;i++)
        	{
        		for(int j=0;j<joueurs.Count;j++)
        		{
        			if(joueurs[j].Rank==i)
        			{
        				((Range)ws.Cells[k,3]).Value= joueurs[j].Nom;
        				((Range)ws.Cells[k,2]).Value= joueurs[j].Rank;
        				((Range)ws.Cells[k,4]).Value= joueurs[j].Score;
        				k++;
        			}
        		}
        	}
        	Range r=ws.Range[((Range)ws.Cells[ligneDep,2]),((Range)ws.Cells[k-1,4])];
        	contourRange(r);
        	r.HorizontalAlignment= XlHAlign.xlHAlignCenter;
        	r=ws.Range[((Range)ws.Cells[ligneDep,3]),((Range)ws.Cells[k-1,3])];
        	contourRange(r);
        	r.Columns.AutoFit();
        	r=ws.Range[((Range)ws.Cells[ligneDep,2]),((Range)ws.Cells[ligneDep,4])];
        	contourRange(r);
        }
        public static List<int> affScoreRankIntermediaire(List<int> points, Worksheet ws, int decx,int nbmatch)
        {
        	List<nuplet<int>> joueurIni=new List<nuplet<int>>();
        	List<nuplet<int>> joueurATrier = new List<nuplet<int>>();
        	List<int> rank= new List<int>(joueurATrier.Count);            
            for (int i = 0; i < points.Count; i++)
            {
            	nuplet<int> temp=(new nuplet<int>(i, points[i]));
            	joueurIni.Add(temp);
                joueurATrier.Add(temp);
                rank.Add(0);
            }
            joueurATrier.Sort(joueurATrier[0].Compare);

            for(int i=0;i<joueurATrier.Count;i++)
            {
            	if(i==0 || points[joueurATrier[i].Val]!= points[joueurATrier[i-1].Val])
            	{
            		rank[joueurIni.IndexOf(joueurATrier[i])]=i+1;
            	}
            	else{
            		rank[joueurIni.IndexOf(joueurATrier[i])]=rank[joueurIni.IndexOf(joueurATrier[i-1])];
            	}
            }
            for(int i=0;i<joueurATrier.Count;i++)
            {
	            ((Range)ws.Cells[decx + 3 + nbmatch + 4, 5 + i]).Value = points[i];
	            ((Range)ws.Cells[decx + 3 + nbmatch + 5, 5 + i]).Value = rank[i];
            }
            return rank;           
	                	
        }
        public static void affichageResultat(ClasseDonnees donnees, Worksheet ws, int decx, int indiceMatch)
        {
        	List<Match> matchs;
            if(indiceMatch<donnees.grillesPrecedente.Count)
            {
                matchs=donnees.grillesPrecedente[indiceMatch].Matchs;
            }
            else
            {
                matchs=donnees.grilleActuelle.Matchs;
            }
                
        	Range r = ((Worksheet)ws).Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2, 4]];
            r.Interior.Color = ColorTranslator.ToOle(ExportExcel.bleu);
            r.Font.Color = ColorTranslator.ToOle(Color.White);
            r.Font.Bold = false;
            r.Merge(true);
            r.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            r.Value = "Grille n°" + (indiceMatch + 1);

            for (int i = 0; i < matchs.Count; i++)
            {
            	((Range)ws.Cells[decx + 3 + i, 3]).Interior.Color = ColorTranslator.ToOle(ExportExcel.bleu);
            	((Range)ws.Cells[decx + 3 + i, 4]).Interior.Color = ColorTranslator.ToOle(ExportExcel.bleu);
            	((Range)ws.Cells[decx + 3 + i, 3]).Font.Color = ColorTranslator.ToOle(Color.White);
            	((Range)ws.Cells[decx + 3 + i, 4]).Font.Color = ColorTranslator.ToOle(Color.White);
            	((Range)ws.Cells[decx + 3 + i, 3]).Font.Bold = true;
            	((Range)ws.Cells[decx + 3 + i, 4]).Font.Bold = true;
            	((Range)ws.Cells[decx + 3 + i, 3]).Value = matchs[i].EquipeDomicile;
            	((Range)ws.Cells[decx + 3 + i, 4]).Value = matchs[i].EquipeExterieure;
            }
            if(indiceMatch<donnees.grillesPrecedente.Count)
            {
	
	            for (int i = 0; i < 6; i++)
	            {
	                Range temp = ((Worksheet)ws).Range[ws.Cells[decx + 3 + matchs.Count + i, 3], ws.Cells[decx + 3 + matchs.Count + i, 4]];
	                temp.Interior.Color = ColorTranslator.ToOle(ExportExcel.bleu);
	                temp.Font.Color = ColorTranslator.ToOle(Color.White);
	                temp.Font.Bold = true;
	                temp.Merge();
	                temp.HorizontalAlignment = XlHAlign.xlHAlignCenter;
	                contourRange(temp);
	            }
	            ((Range)ws.Cells[decx + 3 + matchs.Count, 3]).Value = "Points Pronostiques";
	            ((Range)ws.Cells[decx + 3 + matchs.Count + 1, 3]).Value = "Points Buteurs";
	            ((Range)ws.Cells[decx + 3 + matchs.Count + 2, 3]).Value = "Point Prise de risque";
	            ((Range)ws.Cells[decx + 3 + matchs.Count + 3, 3]).Value = "Score obtenue sur la grille";
	            ((Range)ws.Cells[decx + 3 + matchs.Count + 4, 3]).Value = "Score total";
	            ((Range)ws.Cells[decx + 3 + matchs.Count + 5, 3]).Value = "Rang";
            }

            
            for (int i = 0; i < donnees.joueurs.Count; i++)
            {
                List<Match> matchJoueur;
                if(indiceMatch<donnees.grillesPrecedente.Count)
                {
                    matchJoueur=donnees.joueurs[i].PronostiquesPrecedents[indiceMatch].Matchs;
                }
                else
                {
                    matchJoueur=donnees.joueurs[i].GrilleActuelle.Matchs;
                }
                ((Range)ws.Cells[decx + 2, 5 + i]).Value = donnees.joueurs[i].Nom;
                ((Range)ws.Cells[decx + 2, 5 + i]).Interior.Color = ColorTranslator.ToOle(ExportExcel.bleu);
                ((Range)ws.Cells[decx + 2, 5 + i]).Font.Color = ColorTranslator.ToOle(Color.White);
                ((Range)ws.Cells[decx + 2, 5 + i]).Font.Bold = true;
                for (int j = 0; j < matchs.Count; j++)
                {
                	if(!(indiceMatch<donnees.grillesPrecedente.Count))
                    {
                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Font.Color=ColorTranslator.ToOle(System.Drawing.Color.Black);
                    	if (matchJoueur[j].Pronostique == Prono.Rien)
	                    {
	                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.Black);
	                    }
                    }
                    else if (matchJoueur[j].Pronostique == matchs[j].Pronostique)
                    {
                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Font.Color = ColorTranslator.ToOle(System.Drawing.Color.Green);
                    }
                    else if (matchJoueur[j].Pronostique == Prono.Rien)
                    {
                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.Black);

                    }
                    else
                    {
                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Font.Color = ColorTranslator.ToOle(System.Drawing.Color.Red);
                    }
                    if (indiceMatch<donnees.grillesPrecedente.Count && donnees.joueurs[i].MatchPronoSeul[indiceMatch][j])
                    {
                    	((Range)ws.Cells[decx + 3 + j, 5 + i]).Interior.Color = ColorTranslator.ToOle(System.Drawing.Color.Black);
                    }
                    ((Range)ws.Cells[decx + 3 + j, 5 + i]).Value = matchJoueur[j].PronostiqueString;
                    

                }
                if(indiceMatch<donnees.grillesPrecedente.Count)
                {
                	((Range)ws.Cells[decx + 3 + matchs.Count, 5 + i]).Value = donnees.joueurs[i].ScorePronoParJournee[indiceMatch];
                	((Range)ws.Cells[decx + 3 + matchs.Count + 1, 5 + i]).Value = donnees.joueurs[i].PointButeurParJournee[indiceMatch];
                	((Range)ws.Cells[decx + 3 + matchs.Count + 2, 5 + i]).Value = donnees.joueurs[i].PointBonusSeul[indiceMatch];
                	((Range)ws.Cells[decx + 3 + matchs.Count + 3, 5 + i]).Value = donnees.joueurs[i].PointButeurParJournee[indiceMatch] + donnees.joueurs[i].ScorePronoParJournee[indiceMatch];
                	if(indiceMatch==donnees.grillesPrecedente.Count-1)
                	{
	                	((Range)ws.Cells[decx + 3 + matchs.Count + 4, 5 + i]).Value = donnees.joueurs[i].Score;
	                	((Range)ws.Cells[decx + 3 + matchs.Count + 5, 5 + i]).Value = donnees.joueurs[i].Rank;
                	}
                }

            }
            (((Range)ws.Range[ws.Cells[decx + 2, 5 ],ws.Cells[decx + 2, 5 + donnees.joueurs.Count]]).Columns).AutoFit();
            if(indiceMatch<donnees.grillesPrecedente.Count)
            {
            	((Range)ws.Cells[decx + 2, 5 + donnees.joueurs.Count]).Value = "Resultat";
            	((Range)ws.Cells[decx + 2, 5 + donnees.joueurs.Count]).Interior.Color = ColorTranslator.ToOle(Color.Purple);
            	((Range)ws.Cells[decx + 2, 5 + donnees.joueurs.Count]).Font.Color = ColorTranslator.ToOle(Color.White);

                for (int j = 0; j < donnees.grillesPrecedente[indiceMatch].Matchs.Count; j++)
                {
                	((Range)ws.Cells[decx + 3 + j, 5 + donnees.joueurs.Count]).Value = donnees.grillesPrecedente[indiceMatch].Matchs[j].PronostiqueString;
                	((Range)ws.Cells[decx + 3 + j, 5 + donnees.joueurs.Count]).Interior.Color = ColorTranslator.ToOle(Color.Purple);
                	((Range)ws.Cells[decx + 3 + j, 5 + donnees.joueurs.Count]).Font.Color = ColorTranslator.ToOle(Color.White);
                }
            }


            Range rangeBordure;

			if(indiceMatch<donnees.grillesPrecedente.Count)
            {
				rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2, 3 + donnees.joueurs.Count + 2]];
	            contourRange(rangeBordure);
				rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count, 3 + donnees.joueurs.Count + 2]];
	            contourRange(rangeBordure);
			    rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count + 5, 3 + donnees.joueurs.Count + 1]];
            	contourRange(rangeBordure);
            	rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count + 5, 4]];
            	contourRange(rangeBordure);
            
				for (int i = 0; i < 6; i++)
	            {

	                rangeBordure = ws.Range[ws.Cells[decx + 2 + matchs.Count + i + 1, 3], ws.Cells[decx + 2 + matchs.Count + i + 1, 3 + donnees.joueurs.Count + 1]];
	                contourRange(rangeBordure);
	                rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count + 5 + 10, 3 + donnees.joueurs.Count + 10]];
	            	rangeBordure.HorizontalAlignment = XlHAlign.xlHAlignCenter;
	            }
			}
			else
			{
				rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2, 3 + donnees.joueurs.Count + 1]];
	            contourRange(rangeBordure);
				rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count, 3 + donnees.joueurs.Count + 1]];
				rangeBordure.HorizontalAlignment = XlHAlign.xlHAlignCenter;
	            contourRange(rangeBordure);
            	rangeBordure = ws.Range[ws.Cells[decx + 2, 3], ws.Cells[decx + 2 + matchs.Count , 4]];
            	contourRange(rangeBordure);
			}

                

                
        }
        public static void afficherButeurs(List<Buteur> buteurs, Worksheet ws)
        {
        	for(int i=0;i<buteurs.Count;i++)
        	{
        		((Range)ws.Cells[ i+2,1]).Value=buteurs[i].Nom;
        		for(int j=0;j<buteurs[i].ButParGrille.Count;j++)
        		{
        			if(i==0)
        			{
        				((Range)ws.Cells[ 1,j+2]).Value="Journée "+ (j+1);
        			}
        			((Range)ws.Cells[ i+2,j+2]).Value=buteurs[i].ButParGrille[j];
        		}
        	}
        	Range r=ws.Range[((Range)ws.Cells[1,1]),((Range)ws.Cells[buteurs.Count+1,buteurs[0].ButParGrille.Count+1])];
        	r.Columns.AutoFit();
        	contourRange(r);
        	r.HorizontalAlignment= XlHAlign.xlHAlignCenter;
        	r=ws.Range[((Range)ws.Cells[1,1]),((Range)ws.Cells[1,1])];
        	contourRange(r);

        	r=ws.Range[((Range)ws.Cells[2,2]),((Range)ws.Cells[buteurs.Count+1,buteurs[0].ButParGrille.Count+1])];
        	contourRange(r);
        }
        public static void afficherClassementButeurs(List<Buteur> buteurs, Worksheet ws)
        {
        	for(int i=0;i<buteurs.Count;i++)
        	{
				for(int j=i;j<buteurs.Count;j++)
	        	{
					if(buteurs[i].Buts<buteurs[j].Buts)
					{
						Buteur temp=buteurs[i];
						buteurs[i]=buteurs[j];
						buteurs[j]=temp;
					}
	        	}        	
        	}
        	int temprang=0;
        	int ligneDep=2;
        	int k=ligneDep+1;
        	((Range)ws.Cells[ligneDep,3]).Value="Nom";
        	((Range)ws.Cells[ligneDep,2]).Value="Rang";
        	((Range)ws.Cells[ligneDep,4]).Value="Buts";
        	for(int i=0;i<buteurs.Count;i++)
        	{
        		if(i==0 || buteurs[i].Buts!=buteurs[i-1].Buts)
        		{
        			((Range)ws.Cells[k+i,2]).Value=i+1;
        			temprang=i+1;
        		}
        		else
        		{
        			((Range)ws.Cells[k+i,2]).Value=temprang;
        		}
        		((Range)ws.Cells[k+i,3]).Value=buteurs[i].Nom;
        		((Range)ws.Cells[k+i,4]).Value=buteurs[i].Buts;
        	}
        	Range r=ws.Range[((Range)ws.Cells[ligneDep,2]),((Range)ws.Cells[k+buteurs.Count-1,4])];
        	contourRange(r);
        	r.HorizontalAlignment= XlHAlign.xlHAlignCenter;
        	r=ws.Range[((Range)ws.Cells[ligneDep,3]),((Range)ws.Cells[k+buteurs.Count-1,3])];
        	contourRange(r);
        	r.Columns.AutoFit();
        	r=ws.Range[((Range)ws.Cells[ligneDep,2]),((Range)ws.Cells[ligneDep,4])];
        	contourRange(r);
        	
        	
        }
        public static void creerGraphiques(Workbook wb, ClasseDonnees donnees)
        {
        	List<string> noms=new List<string>(){"Evolution score","Evolution prono pur","Evolution points buteurs", "Evolution points risque","Evolution rang"};
        	for(int i=0;i<5;i++)
        	{
	        	Chart graph=(Chart)wb.Charts.Add();
	        	graph.Type= (int)XlChartType.xlLine;
	        	graph.Name=noms[i];
	        	SeriesCollection seriesCollection = (SeriesCollection)graph.SeriesCollection();
	        	while(seriesCollection.Count>0)
	        	{
	        		((Series)seriesCollection.Item(1)).Delete();
	        	}
	        	for(int j=0;j<donnees.joueurs.Count;j++)
	        	{
	        		string col=toColonne(j*5+i+1);
	        		Series s=seriesCollection.Add("'Scores Cummulés'!$"+col+"$1:$"+col+"$"+donnees.grillesPrecedente.Count);
	        		s.Name=donnees.joueurs[j].Nom;
	        	}
        	}

        	/*
        		    ActiveChart.SeriesCollection.NewSeries
    ActiveChart.SeriesCollection(2).Name = "=""LALA"""
    ActiveChart.SeriesCollection(2).Values = "='Scores Cummulés'!$F$1:$F$3"**/
        }
        private static string toColonne(int i)
        {
        	string str="";
        	int itemp=int.MaxValue;
        	while(itemp>26)
        	{
        		str=((char)('A'+(i-1)%26))+str;
        		itemp=i;
        		i=(i-1)/26;
        	}
        	return str;
        }
        private static void contourRange(Range r)
        {
            r.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
            r.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
            r.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
            r.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
            r.Borders.get_Item(XlBordersIndex.xlEdgeTop).Color = ColorTranslator.ToOle(noir);
            r.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Color = ColorTranslator.ToOle(noir);
            r.Borders.get_Item(XlBordersIndex.xlEdgeRight).Color = ColorTranslator.ToOle(noir);
            r.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Color = ColorTranslator.ToOle(noir);
        }
    }
}
