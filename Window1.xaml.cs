﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotoFoot
{

    public class ClasseCreationJoueur
    {
        private static List<string> buteurChapeau1;
        private static List<string> buteurChapeau2;
        public static void setButeur1(List<Buteur> buteurChapeau1)
        {
            List<string> temp=new List<string>();
            foreach (Buteur b in buteurChapeau1)
            {
                temp.Add(b.Nom);
            }
            ClasseCreationJoueur.buteurChapeau1 = temp;
        }
        public static void setButeur2(List<Buteur> buteurChapeau2)
        {
            List<string> temp = new List<string>();
            foreach (Buteur b in buteurChapeau2)
            {
                temp.Add(b.Nom);
            }
            ClasseCreationJoueur.buteurChapeau2 = temp;
        }
        public static List<string> ButeurChapeau1
        {
            get
            {
                return buteurChapeau1;
            }
        }
        public static List<string> ButeurChapeau2
        {
            get
            {
                return buteurChapeau2;
            }
        }
        public string nom
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string EmailPerso
        {
            get;
            set;
        }
        public string AMiseString
        {
            get;
            set;
        }
        public string APayeString
        {
            get;
            set;
        }
        public bool AMise
        {
            get
            {
                return AMiseString == "Oui";
            }
        }
        public bool APaye
        {
            get
            {
                return APayeString == "Oui";
            }
        }
        public string Premier
        {
            get;
            set;
        }
        public string Second
        {
            get;
            set;
        }
        public string Troisieme
        {
            get;
            set;
        }
        public string Antepenultiemme
        {
            get;
            set;
        }
        public string AvantDernier
        {
            get;
            set;
        }
        public string Dernier
        {
            get;
            set;
        }
        public string buteur1
        {
            get;
            set;
        }
        public string buteur2
        {
            get;
            set;
        }
        public string buteur3
        {
            get;
            set;
        }
        public ClasseCreationJoueur()
        {

        }

        public ClasseCreationJoueur(List<string> list)
        {
            nom = list[0];
            Email = list[1];
            EmailPerso = list[2];
            AMiseString = list[3];
            APayeString = list[4];
            buteur1 = list[5];
            buteur2 = list[6];
            buteur3 = list[7];
            Premier = list[8]; 
            Second = list[9];
            Troisieme = list[10];
            Antepenultiemme = list[11];
            AvantDernier = list[12];
            Dernier = list[13];
        }
    }
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {

        private List<ClasseCreationJoueur> joueurACreer;
        private ClasseCreationJoueur temp;
        private MainWindow main;
        public Window1(MainWindow main,List<Buteur> buteurChapeau1, List<Buteur> buteurChapeau2)
        {
            this.main = main;
            temp = new ClasseCreationJoueur();
            ClasseCreationJoueur.setButeur1(buteurChapeau1);
            ClasseCreationJoueur.setButeur2(buteurChapeau2);
            this.joueurACreer = new List<ClasseCreationJoueur>();
            InitializeComponent();
            

            
            grilleJoueur.ItemsSource = joueurACreer;
            comboButeur1.ItemsSource = ClasseCreationJoueur.ButeurChapeau1;
            comboButeur2.ItemsSource = ClasseCreationJoueur.ButeurChapeau2;
            /*
            DataGridTemplateColumn dd = new DataGridTemplateColumn();
            ComboBox cb=new ComboBox();
            DataTemplate textTemplate = new DataTemplate();
            FrameworkElementFactory comboFactory = new FrameworkElementFactory(typeof(ComboBox));
            comboFactory.SetValue(ComboBox.IsTextSearchEnabledProperty, true);
            comboFactory.SetValue(ComboBox.ItemsSourceProperty, ClasseCreationJoueur.ButeurChapeau1);
            Binding comboBind = new Binding("buteur1");
            comboBind.Mode = BindingMode.OneWay;
            comboFactory.SetBinding(ComboBox.SelectedItemProperty, comboBind);
            textTemplate.VisualTree = comboFactory;
            dd.CellTemplate = textTemplate;
            grilleJoueur.Columns.Add(dd);
            */
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            main.CreeJoueurs(joueurACreer);
            Close();
        }

        private void grilleJoueur_KeyDown(object sender, KeyEventArgs e)
        {
            if ( e.Key==Key.V && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                try
                {
                    string st =Clipboard.GetText();
                    List<string> ligneJoueur = new List<string>(st.Split(new string[]{"\r\n"},int.MaxValue,StringSplitOptions.RemoveEmptyEntries));
                    List<List<string>> ligneColonneJoueur= new List<List<string>>();
                    foreach (string s in ligneJoueur)
                    {
                        ligneColonneJoueur.Add(new List<string>(s.Split(new string[] { "\t" }, int.MaxValue, StringSplitOptions.None)));
                    }
                    for (int i = 0; i < ligneColonneJoueur.Count; i++)
                    {
                        joueurACreer.Add(new ClasseCreationJoueur(ligneColonneJoueur[i]));
                    }
                    grilleJoueur.ItemsSource = null;
                    grilleJoueur.ItemsSource = joueurACreer;
                }
                catch
                {
                    MessageBox.Show("Le format des données collé ne correspond pas au format attendue","Erreur",MessageBoxButton.OK,MessageBoxImage.Error);
                }
            }
        }
    }
}
