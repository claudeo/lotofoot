﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.IO;
using Outlook = Microsoft.Office.Interop.Outlook;
namespace LotoFoot
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>

    public partial class MainWindow : Window
	{
		private ClasseDonnees classeDonnee;
		public MainWindow()
		{
			InitializeComponent();
			string path = "Myfile.bin";
			if (File.Exists(path))
			{
				classeDonnee = ClasseDonnees.charger(path);
			}
			else
			{
				classeDonnee = new ClasseDonnees();
			}
			if(classeDonnee.grilleActuelle==null)
			{
				menu.Items.Remove(renseignerResultatMenu);
				menu.Items.Remove(genererMailRappelMenu);
			}
			else
			{
				menu.Items.Remove(ajouterGrilleMenu);
			}
			menu.Items.Remove(creerListeJoueurMenu);

		}

		private void MenuItem_Click_1(object sender, RoutedEventArgs e)
		{
			List<Buteur> bt1 = new List<Buteur>();
			List<Buteur> bt2 = new List<Buteur>();
			foreach (Buteur b in classeDonnee.buteurChapeau1.Values)
			{
				bt1.Add(b);
			}

			foreach (Buteur b in classeDonnee.buteurChapeau2.Values)
			{
				bt2.Add(b);
			}

			Window1 w = new Window1(this, bt1, bt2);
			w.Show();
		}

		private void MenuItem_Click_2(object sender, RoutedEventArgs e)
		{
			List<Buteur> bt1 = new List<Buteur>();
			List<Buteur> bt2 = new List<Buteur>();
			foreach (Buteur b in classeDonnee.buteurChapeau1.Values)
			{
				bt1.Add(b);
			}

			foreach (Buteur b in classeDonnee.buteurChapeau2.Values)
			{
				bt2.Add(b);
			}

			GestionButeurs g = new GestionButeurs(this, bt1, bt2);
			g.ShowDialog();
		}

		public void CreeJoueurs(List<ClasseCreationJoueur> joueurACreer)
		{
			if (classeDonnee.joueurs.Count == 30)
			{
				int i = 0;
				foreach (Joueur j in classeDonnee.joueurs)
				{
					j.appliquerJoueur(joueurACreer[i]);
					i++;
				}
			}
			else if (classeDonnee.joueurs == null || MessageBox.Show("Il existe deja des données de joueurs, voulez vous les remplacer?", "Comfirmation", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
			{
				classeDonnee.joueurs = new List<Joueur>();
				List<string> buteurchapeau3 = new List<string>(classeDonnee.buteurChapeau3.Keys);
				foreach(ClasseCreationJoueur j in joueurACreer)
				{
					if (buteurchapeau3.IndexOf(j.buteur3) < 0)
					{
						classeDonnee.buteurChapeau3.Add(j.buteur3, new Buteur(j.buteur3));
						buteurchapeau3 = new List<string>(classeDonnee.buteurChapeau3.Keys);
					}

					classeDonnee.joueurs.Add(new Joueur(j));
				}
			}
		}

		private void MenuItem_Click_3(object sender, RoutedEventArgs e)
		{
			if (classeDonnee.grilleActuelle != null)
			{
				MessageBox.Show("Vous devez d'abord renseigner la grille actuelle", "Erreur", MessageBoxButton.OK, MessageBoxImage.Stop);
			}
			else
			{
				EntreeGrille eg = new EntreeGrille(this);
				eg.ShowDialog();
			}
			if(classeDonnee.grilleActuelle!=null)
			{
				menu.Items.Remove(ajouterGrilleMenu);
				menu.Items.Insert(1,renseignerResultatMenu);
				menu.Items.Insert(3,genererMailRappelMenu);
			}
		}

		public void AjouterGrille(System.Collections.Generic.List<Match> matchs)
		{
			if (classeDonnee.grilleActuelle == null)
			{
				classeDonnee.grilleActuelle = new Grille(matchs);
				foreach (Joueur j in classeDonnee.joueurs)
				{
					j.GrilleActuelle = new Grille(dupliquer(matchs));
				}
			}
		}

		private List<Match> dupliquer(List<Match> match)
		{
			List<Match> rst = new List<Match>();
			for (int i = 0; i < match.Count; i++)
			{
				rst.Add(new Match(match[i]));
			}

			return rst;
		}

		private void MenuItem_Click_4(object sender, RoutedEventArgs e)
		{
			List<Buteur> buteurs = new List<Buteur>(classeDonnee.buteurChapeau1.Values);
			buteurs.AddRange(classeDonnee.buteurChapeau2.Values);
			buteurs.AddRange(classeDonnee.buteurChapeau3.Values);
			if (classeDonnee.grilleActuelle != null)
			{
				RemplissageGrille rp = new RemplissageGrille(this, classeDonnee.grilleActuelle, classeDonnee.joueurs, buteurs);
				rp.ShowDialog();
			}
		}

		internal void misAJourGrilleComplete()
		{
			List<bool> estSeul=new List<bool>();
			for(int i = 0;i<classeDonnee.grilleActuelle.Matchs.Count;i++)
			{
				int count = 0;
				foreach (Joueur j in classeDonnee.joueurs)
				{
					if(j.GrilleActuelle.Matchs[i].Pronostique == classeDonnee.grilleActuelle.Matchs[i].Pronostique)
					{
						count++;
					}
				}

				if(count == 1)
				{
					estSeul.Add(true);
				}
				else
				{
					estSeul.Add(false);
				}
			}

			List<bool> aJoue = new List<bool>();
			foreach (Joueur j in classeDonnee.joueurs)
			{

                aJoue.Add(j.calculResultatGrille(classeDonnee.grilleActuelle, estSeul));
			}
            int minParis=int.MaxValue;
            int minParisButteurs=int.MaxValue;
            int maxParisButteurs=int.MinValue;
            int x=0;
            int indiceGrille = classeDonnee.joueurs[0].PronostiquesPrecedents.Count-1;
			foreach (Joueur j in classeDonnee.joueurs)
			{
                if (aJoue[x])
                {
                    minParis = Math.Min(minParis, j.ScorePronoParJournee[indiceGrille]);
                    int scoreTotal=j.ScorePronoParJournee[indiceGrille] + j.PointButeurParJournee[indiceGrille] + j.PointBonusSeul[indiceGrille];
                    minParisButteurs = Math.Min(minParisButteurs, scoreTotal );
                    maxParisButteurs = Math.Max(maxParisButteurs, scoreTotal);
                }
                x++;
			}
            bool minChange=minParisButteurs<classeDonnee.scoreMin;
            bool maxChange = maxParisButteurs > classeDonnee.scoreMax;
            x = 0;
            foreach (Joueur j in classeDonnee.joueurs)
            {
                if (aJoue[x])
                {
                    j.verifierBunusMalus(minChange, minParisButteurs, maxChange, maxParisButteurs);
                }
                else
                {
                    j.appliquerScoreGrilleNonRendue(minParis - 1);
                }
                x++;
            }
			classeDonnee.grillesPrecedente.Add(classeDonnee.grilleActuelle);
			classeDonnee.grilleActuelle = null;
            List<nuplet<Joueur>> joueurATrier = new List<nuplet<Joueur>>();
            for (int i = 0; i < classeDonnee.joueurs.Count; i++)
            {
                joueurATrier.Add(new nuplet<Joueur>(classeDonnee.joueurs[i], classeDonnee.joueurs[i].Score));
            }
            joueurATrier.Sort(joueurATrier[0].Compare);
            for (int i = 0; i < classeDonnee.joueurs.Count; i++)
            {
            	if(i==0 || joueurATrier[i].Val.Score!=joueurATrier[i-1].Val.Score)
            	{
                	joueurATrier[i].Val.Rank = i + 1;
            	}
            	else
            	{
            		joueurATrier[i].Val.Rank = joueurATrier[i-1].Val.Rank;
            	}
            }
            if(classeDonnee.grilleActuelle==null)
            {

				menu.Items.Remove(renseignerResultatMenu);
				menu.Items.Remove(genererMailRappelMenu);
				menu.Items.Insert(1,ajouterGrilleMenu);
            }
		}


        internal void misAJourButeurs(List<Buteur> buteur1,List<Buteur> buteur2)
        {

            retirerButeur(classeDonnee.buteurChapeau1);
            retirerButeur(classeDonnee.buteurChapeau2);
            misAjourChapeau(buteur1, classeDonnee.buteurChapeau1);
            misAjourChapeau(buteur2, classeDonnee.buteurChapeau2);

        }
        private void retirerButeur(Dictionary<string,Buteur> buteurs)
        {
            List<string> nom = new List<string>(buteurs.Keys);
            for (int i = 0; i < nom.Count; i++)
            {
                if (nom[i] != buteurs[nom[i]].Nom)
                {
                    buteurs.Remove(nom[i]);
                }
            }
        }
        private void misAjourChapeau(List<Buteur> listeButeur, Dictionary<string, Buteur> dicoButeur)
        {
            List<string> nomButeur = new List<string>(dicoButeur.Keys);
            foreach (Buteur b in listeButeur)
            {

                if (nomButeur.IndexOf(b.Nom) < 0)
                {
                    dicoButeur.Add(b.Nom, b);
                }
            }
        }

        internal void appliquerButeur(System.Collections.Generic.List<Buteur> buteurs)
        {
            foreach (Joueur j in classeDonnee.joueurs)
            {
                j.appliquerPointButeur(classeDonnee.buteurChapeau1, classeDonnee.buteurChapeau2, classeDonnee.buteurChapeau3); 
            }

            ChangerMatchButeur(classeDonnee.buteurChapeau1);
            ChangerMatchButeur(classeDonnee.buteurChapeau2);
            ChangerMatchButeur(classeDonnee.buteurChapeau3);

        }

        private void ChangerMatchButeur(Dictionary<string,Buteur> buteurs)
        {
            foreach(Buteur b in buteurs.Values)
            {
                b.ChangerMatch();
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            classeDonnee.sauvegarder();
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
        	
            ExportExcel.creerExcel(classeDonnee,this, classeDonnee.grilleActuelle==null,"");
        }
        private void MailGrille_Click(object sender, RoutedEventArgs e)
        {
        	
        	genererMailEnvoiGrille();
        }

        private void MailResultat_Click(object sender, RoutedEventArgs e)
        {
        	
        	genererMailPronoResultat();
        }
        private void EditJoueur_Click(object sender, RoutedEventArgs e)
        {
        	SelectionJoueur sj=new SelectionJoueur(classeDonnee,false);
        	sj.ShowDialog();
        }
       	private void SupprimerJoueur_Click(object sender, RoutedEventArgs e)
        {
        	SelectionJoueur sj=new SelectionJoueur(classeDonnee,true);
        	sj.ShowDialog();
        }
        internal void showM(string p)
        {
            MessageBox.Show(p);
        }

        public void genererMailEnvoiGrille()
		{
			Outlook.Application ap = new Outlook.Application();
			Outlook.MailItem mail = ap.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
			mail.Subject = "Prive: LotoFoot";
			bool estRappel=false;
			foreach(Joueur j in classeDonnee.joueurs)
			{
				if(j.APronostique)
				{
					estRappel=true;
				}
			}
			if(estRappel)
			{
				mail.HTMLBody+= "Petit rappel :) <br />" ;
			}
			else
			{
				mail.HTMLBody+= "Bonjour à tous,<br />Voici la grille du Week-end.<br />Cordialement";
			}
			
			mail.HTMLBody += "<table style=\"border-collapse:collapse\">";
			List<Match> matchs = classeDonnee.grilleActuelle.Matchs;
			for(int i = 0;i<matchs.Count;i++)
			{
				mail.HTMLBody += "<tr> <td style=\"background-color: #000080; color: #FFFFFF\">" + matchs[i].EquipeDomicile + "</td> <td style=\"background-color: #000080; color: #FFFFFF\">1N2</td> <td style=\"background-color: #000080; color: #FFFFFF; text-align: right \">" + matchs[i].EquipeExterieure + "</td> </tr> ";
			}

			mail.HTMLBody+= "</table> ";
			Outlook.AddressEntry currentUser = ap.Session.CurrentUser.AddressEntry;
			foreach(Joueur j in classeDonnee.joueurs)
			{
				if(!j.APronostique)
				{
					if(j.emailPerso!= "")
					{
						mail.Recipients.Add(j.emailPerso);
					}

					if(j.email!= "")
					{
						mail.Recipients.Add(j.email);
					}
				}
			}
			mail.HTMLBody = mail.HTMLBody.Replace("Times New Roman", "Calibri"); 
			string st=mail.HTMLBody;
			mail.Recipients.ResolveAll();
			mail.Display(true);
		}

		public void genererMailPronoResultat()
		{
			Outlook.Application ap = new Outlook.Application();
			Outlook.MailItem mail = ap.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
			mail.Subject = "Prive: LotoFoot";

			foreach(Joueur j in classeDonnee.joueurs)
			{
				if(j.emailPerso!= "")
				{
					mail.Recipients.Add(j.emailPerso);
				}

				if(j.email!= "")
				{
					mail.Recipients.Add(j.email);
				}
			}

			var dialog = new System.Windows.Forms.FolderBrowserDialog();
			System.Windows.Forms.DialogResult result = dialog.ShowDialog();
			string nomFichier;
			if(classeDonnee.grilleActuelle == null)
			{
				nomFichier = "Resultat Grille n"+ classeDonnee.grillesPrecedente.Count+".xlsx";
			}
			else
			{
				nomFichier = "Pronostique n"+ (classeDonnee.grillesPrecedente.Count+1)+".xlsx";
			}
			ExportExcel.creerExcel(classeDonnee, this, classeDonnee.grilleActuelle == null,dialog.SelectedPath + "\\"+nomFichier);
			mail.Attachments.Add(dialog.SelectedPath + "\\"+nomFichier);
			mail.HTMLBody = mail.HTMLBody.Replace("Times New Roman", "Calibri"); 
			mail.Recipients.ResolveAll();
			mail.Display(true);
		}
		
    }
    public class nuplet<T>:IComparer<nuplet<T>>
    {
        private int i;
        private T obj;
        public T Val
        {
            get
            {
                return obj;
            }
        }
        public nuplet(T val, int i)
        {
            this.obj=val;
            this.i=i;
        }


        public int Compare(nuplet<T> a,nuplet<T> b)
        {
            int rst = 0;
            if (a.i > b.i)
            {
                rst = -1;
            }
            else if(a.i < b.i)
            {
                rst = 1;
            }
            return rst;
        }



    }
}
