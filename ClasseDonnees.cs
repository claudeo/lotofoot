﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LotoFoot
{
	[Serializable]
	public class ClasseDonnees
	{
		public Dictionary<string, Buteur> buteurChapeau1;
		public Dictionary<string, Buteur> buteurChapeau2;
		public Dictionary<string, Buteur> buteurChapeau3;
		public List<Joueur> joueurs;
		public List<Grille> grillesPrecedente;
		public Grille grilleActuelle;
		public int scoreMin=0;
        public int scoreMax = int.MaxValue;
        public ClasseDonnees()
        {
            buteurChapeau1 = new Dictionary<string, Buteur>();
            buteurChapeau2 = new Dictionary<string, Buteur>();
            buteurChapeau3 = new Dictionary<string, Buteur>();
            grillesPrecedente = new List<Grille>();
        }

        public void sauvegarder()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }
        public static ClasseDonnees charger(string path)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            ClasseDonnees obj = (ClasseDonnees)formatter.Deserialize(stream);
            stream.Close();
            return obj;
        }
        
        public List<Buteur> Buteurs
        {
        	get
        	{
        		List<Buteur> buteurs = new List<Buteur>();
        		buteurs.AddRange(buteurChapeau1.Values);
        		buteurs.AddRange(buteurChapeau2.Values);
        		buteurs.AddRange(buteurChapeau3.Values);
        		return buteurs;
        	}
        }
    }
}
