﻿/*
 * Created by SharpDevelop.
 * User: parent.claude
 * Date: 08/28/2015
 * Time: 11:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using LotoFoot;

namespace Lotofoot
{
	/// <summary>
	/// Interaction logic for Window2.xaml
	/// </summary>
	public partial class RecupString : Window
	{
		private ClasseDonnees donnees;
		public RecupString(ClasseDonnees donnees)
		{
			InitializeComponent();
			this.donnees=donnees;
		}
		
		void button1_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				donnees.buteurChapeau3.Add(textbox.Text, new Buteur(textbox.Text, donnees.Buteurs[0].ButParGrille.Count));
				Close();
			}
			catch
			{
				MessageBox.Show("Le buteur que vous essayé d'ajouter existe déja");
			}
			
		}
	}
}