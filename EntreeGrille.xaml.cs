﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;
using System.Net;
using System.IO;
namespace LotoFoot
{
    /// <summary>
    /// Logique d'interaction pour EntreeGrille.xaml
    /// </summary>
    public partial class EntreeGrille : Window
    {
        private MainWindow main;
        private List<Match> matchs;
        public EntreeGrille(MainWindow main)
        {
            this.main = main;
            InitializeComponent();
            matchs = new List<Match>();

            grille.ItemsSource = matchs;
            
        }

        public void chargerFromInternet(DateTime dt)
        {
            string jours = dt.ToString("dddd",new CultureInfo("fr-FR"));
            jours.Substring(0,1).ToUpper();
            int day = dt.Day;
            string mois = ((Mois)dt.Month).ToString();
            int annee = dt.Year;
            string codeHtml;
            try
            {

	        	HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://europroxy.eu/browse.php?u=http%3A%2F%2Fwww.pronosoft.com%2Ffr%2Flotofoot%2Fprochaines-grilles.htm");
				HttpWebResponse response = (HttpWebResponse)request.GetResponse ();
				Stream receiveStream = response.GetResponseStream ();
	    		StreamReader readStream = new StreamReader (receiveStream, Encoding.UTF8);
	   	 		codeHtml= readStream.ReadToEnd();
            }
            catch(Exception e)
            {
            	using (WebClient client = new WebClient()) // classe WebClient hérite IDisposable
	            {
            		client.Proxy=new WebProxy("proxy.matmut.fr",80);
            		client.Proxy.Credentials=CredentialCache.DefaultCredentials;
            		codeHtml = client.DownloadString("http://europroxy.eu/browse.php?u=http%3A%2F%2Fwww.pronosoft.com%2Ffr%2Flotofoot%2Fprochaines-grilles.htm");
            	}
            }
            string s="Avant le "+jours + " " + day+ " " + mois;
            int indDep=codeHtml.IndexOf(s,StringComparison.InvariantCultureIgnoreCase);
            int indMax = codeHtml.IndexOf("<thead>",indDep);
            int ind = codeHtml.IndexOf("\"equipe1\"",indDep);
            while (ind < indMax)
            {
                int indFinE1 = codeHtml.IndexOf("</td>", ind);
                matchs.Add(new Match());
                matchs[matchs.Count-1].EquipeDomicile=codeHtml.Substring(ind + 10, indFinE1  -(ind+10));
                ind= codeHtml.IndexOf("\"equipe2\"",ind);
                int indFinE2 = codeHtml.IndexOf("</td>", ind);
                matchs[matchs.Count - 1].EquipeExterieure = codeHtml.Substring(ind + 10, indFinE2 - (ind + 10));
                ind = codeHtml.IndexOf("\"equipe1\"", ind);
            }
            grille.Items.Refresh();

        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            main.AjouterGrille(matchs);
            Close();
        }
        public enum Mois
        {
            Janvier=1, Fevrier=2, Mars=3, Avril=4, Mai=5, Juin=6, Juillet=7, Août=8, Septembre=9, Octobre=10, Novembre=11, Decembre=12
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            selectionDate sc = new selectionDate(this);
            sc.ShowDialog();
        }
    }
}
