﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotoFoot
{
    /// <summary>
    /// Logique d'interaction pour RemplissageGrille.xaml
    /// </summary>
    public partial class RemplissageGrille : Window
    {
        private Grille grilleProno;
        private List<Joueur> joueurs;
        private MainWindow main;
        private List<string> choixCB;
        public bool buteurRenseigne
        {
            get;
            set;
        }
        private List<Buteur> buteurs;
        private Grille grilleActuelle;
        public RemplissageGrille(MainWindow main, Grille grilleProno, List<Joueur> joueurs, List<Buteur> buteurs)
        {
            InitializeComponent();
            this.buteurs = buteurs;
            choixCB = new List<string>();
            choixCB.Add("RemplirResultat");
            foreach (Joueur j in joueurs)
            {
                choixCB.Add(j.Nom);
            }
            this.grilleProno=grilleProno;
            this.joueurs = joueurs;
            this.main = main;


            grille.IsEnabled = false;

            grille.ItemsSource = grilleProno.Matchs;

            Pronostique.ItemsSource = Match.prono;
            combo.ItemsSource = choixCB;
            grilleActuelle = grilleProno;
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            grille.IsEnabled = true;
            if (((ComboBox)sender).SelectedIndex == 0)
            {
                grille.ItemsSource = grilleProno.Matchs;
                grilleActuelle = grilleProno;
            }
            else
            {
                grille.ItemsSource = joueurs[((ComboBox)sender).SelectedIndex - 1].GrilleActuelle.Matchs;
                grilleActuelle = joueurs[((ComboBox)sender).SelectedIndex - 1].GrilleActuelle;
            }
        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (Joueur j in joueurs)
            {
                j.validerGrille();
            }
            bool grilleValide=true;
            foreach(Match m in grilleProno.Matchs)
            {
                grilleValide= grilleValide && m.Pronostique!=Prono.Rien;
            }
            if (grilleValide)
            {
                bool b = true;
                int i = 0;
                while (i < joueurs.Count && b)
                {
                    b = joueurs[i].APronostique;
                    i++;
                }
                if (b || MessageBox.Show("Cetains joueurs n'ont pas pronostiqués, voulez vous tout de même valider les résultats?", "Attention", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    RenseignerButeurs rb = new RenseignerButeurs(buteurs, main, this);
                    rb.ShowDialog();
                    if (buteurRenseigne)
                    {
                        main.misAJourGrilleComplete();
                        Close();
                    }
                }
            }
            else
            {
                Close();
            }
        }

        private void grille_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.V && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && grille.IsEnabled==true)
            {
                try
                {
                    string st = Clipboard.GetText();
                    List<string> listeCellules = new List<string>(st.Split(new string[] { "\r\n" }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries));
                    if(listeCellules.Count==grilleActuelle.Matchs.Count)
                    {
                    	for (int i = 0; i < grilleActuelle.Matchs.Count; i++)
	                    {
                    		grilleActuelle.Matchs[i].PronostiqueString = listeCellules[i].Split('\t')[1];
	                    }
                    		                    
                    }
                    else
                    {
	                    for (int i = 0; i < grilleActuelle.Matchs.Count; i++)
	                    {
	                        grilleActuelle.Matchs[i].PronostiqueString = listeCellules[3*i+1];
	                    }
                    }
                    grille.ItemsSource = null;
                    grille.ItemsSource = grilleActuelle.Matchs;
                }
                catch
                {
                    MessageBox.Show("Le format des données collé ne correspond pas au format attendue", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
