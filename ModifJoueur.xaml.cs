﻿/*
 * Created by SharpDevelop.
 * User: parent.claude
 * Date: 24/08/2015
 * Time: 08:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Lotofoot;

namespace LotoFoot
{
	/// <summary>
	/// Interaction logic for Window2.xaml
	/// </summary>
	public partial class Window2 : Window
	{
		private Joueur j;
		private ClasseDonnees donnees;
		public Window2(Joueur j,ClasseDonnees donnees)
		{
			InitializeComponent();
			this.j=j;
			textEmail.Text=j.email;
			TextMailPerso.Text=j.emailPerso;
			textNom.Text=j.Nom;
			this.donnees=donnees;
			comboButeur1.ItemsSource=donnees.buteurChapeau1.Keys;
			comboButeur2.ItemsSource=donnees.buteurChapeau2.Keys;
			comboButeur3.ItemsSource=donnees.buteurChapeau3.Keys;
			comboButeur1.SelectedItem=j.ButeursActuels[0];
			comboButeur2.SelectedItem=j.ButeursActuels[1];
			comboButeur3.SelectedItem=j.ButeursActuels[2];
		}
		
		void button1_Click(object sender, RoutedEventArgs e)
		{
			j.emailPerso=TextMailPerso.Text;
			j.email=textEmail.Text;
			int nbChangement=0;
			if( comboButeur1.SelectedItem!=j.ButeursActuels[0])
			{
				nbChangement++;
			}
			if( comboButeur2.SelectedItem!=j.ButeursActuels[1])
			{
				nbChangement++;
			}
			if( comboButeur3.SelectedItem!=j.ButeursActuels[2])
			{
				nbChangement++;
			}
			if(nbChangement>0)
			{
				if(j.AChangeButeur==true)
				{
					if(MessageBox.Show("Le joueur à déjà changé de buteur, le ou les changement(s) correspondent bien à un ou des transferts à l'étrangé?","Attention",MessageBoxButton.YesNo,MessageBoxImage.Information)== MessageBoxResult.Yes)
					{
						j.ButeursActuels[0]=(string)comboButeur1.SelectedItem;
						j.ButeursActuels[1]=(string)comboButeur2.SelectedItem;
						j.ButeursActuels[2]=(string)comboButeur3.SelectedItem;
					}
					
				}
				else
				{
					if(nbChangement>1 && MessageBox.Show("Vous faites plusieurs changement de buteurs, le ou les changement(s) correspondent bien à un ou des transferts à l'étrangé?","Attention",MessageBoxButton.YesNo,MessageBoxImage.Information)== MessageBoxResult.Yes|| nbChangement==1)
					{
						if(MessageBox.Show("Y a il un changement de buteur qui ne correspond pas à un départ à l'étrangé","Attention",MessageBoxButton.YesNo,MessageBoxImage.Information)== MessageBoxResult.Yes)
						{
							j.AChangeButeur=true;
						}
						j.ButeursActuels[0]=(string)comboButeur1.SelectedItem;
						j.ButeursActuels[1]=(string)comboButeur2.SelectedItem;
						j.ButeursActuels[2]=(string)comboButeur3.SelectedItem;
						
					}
					
				}
				
			}
			Close();
		}
		
		void button2_Click(object sender, RoutedEventArgs e)
		{
			RecupString rs= new RecupString(donnees);
			rs.ShowDialog();
			comboButeur3.ItemsSource=null;
			comboButeur3.ItemsSource=donnees.buteurChapeau3.Keys;
			comboButeur3.SelectedItem=j.ButeursActuels[2];
		}
	}
}