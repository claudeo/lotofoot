﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotoFoot
{
    /// <summary>
    /// Logique d'interaction pour RenseignerButeurs.xaml
    /// </summary>
    public partial class RenseignerButeurs : Window
    {
        private List<Buteur> buteurs;
        private MainWindow main;
        private RemplissageGrille rp;
        public RenseignerButeurs(List<Buteur> buteurs, MainWindow main, RemplissageGrille rp)
        {
            this.buteurs = buteurs;
            this.main = main;
            this.rp = rp;
            InitializeComponent();
            dg1.ItemsSource = buteurs;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            rp.buteurRenseigne = true;
            main.appliquerButeur(buteurs);
            Close();
        }
    }
}
