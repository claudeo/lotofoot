﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotoFoot
{
    [Serializable]
    public class Buteur
    {
        public Buteur()
        {
            ButParGrille = new List<int>();
        }
        public Buteur(string nom)
        {
            this.Nom = nom;
            ButParGrille = new List<int>();
        }
        public Buteur(string nom, int nbJournee)
        {
        	this.Nom = nom;
            ButParGrille = new List<int>();
            for(int i=0;i<nbJournee;i++)
            {
            	ButParGrille.Add(0);
            }
        }
        public string Nom
        {
            get;
            set;
        }
        public int ButGrilleActuelle
        {
            get;
            set;
        }
        public List<int> ButParGrille
        {
            get;
            set;
        }

        public void ChangerMatch()
        {
            ButParGrille.Add(ButGrilleActuelle);
            ButGrilleActuelle = 0;
        }
        public int Buts
        {
        	get
        	{
        		int nbBut=0;
        		for(int i=0;i<ButParGrille.Count;i++)
        		{
        			nbBut+=ButParGrille[i];
        		}
        		return nbBut;
        	}
        }
    }
}
