﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotoFoot
{
    /// <summary>
    /// Logique d'interaction pour selectionDate.xaml
    /// </summary>
    public partial class selectionDate : Window
    {
        private EntreeGrille eg;
        public selectionDate(EntreeGrille eg)
        {
            this.eg = eg;
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (datePicker.SelectedDate != null)
            {
                eg.chargerFromInternet((DateTime)datePicker.SelectedDate);
                Close();
            }
            else
            {
                MessageBox.Show("La date n'est pas renseignée");
            }
        }
    }
}
