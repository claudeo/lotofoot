﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotoFoot
{
    [Serializable]
    public class Grille
    {
        private List<Match> matchs;
        public List<Match> Matchs
        {
            get { return matchs; }
        }
        public Grille(List<Match> matchs)
        {
            this.matchs = matchs;
        }
    }
    [Serializable]
    public class Match
    {
        public static List<string> prono = new List<string>() { "","1", "2", "N" };

        public Match(Match match)
        {
            EquipeDomicile = match.EquipeDomicile;
            EquipeExterieure = match.EquipeExterieure;
            Pronostique = match.Pronostique;

        }
        public Match()
        {
        }

        public string EquipeDomicile
        {
            get;
            set;
        }
        public string EquipeExterieure
        {
            get;
            set;
        }
        public Prono Pronostique
        {
            get;
            set;
        }
        public string PronostiqueString
        {
            get
            {
                string rst = "";
                if (Pronostique == Prono.Domicile)
                {
                    rst = "1";
                }
                else if (Pronostique == Prono.Exterieur)
                {
                    rst = "2";
                }
                else if(Pronostique == Prono.Nul)
                {
                    rst = "N";
                }
                return rst;
            }
            set
            {
                if (value == "1")
                {
                    Pronostique = Prono.Domicile;
                }
                else if (value == "2")
                {
                    Pronostique = Prono.Exterieur;
                }
                else if (value == "N")
                {
                    Pronostique = Prono.Nul;
                }
                else
                {
                    Pronostique = Prono.Rien;
                }
            }
        }
       
    }

    [Serializable]
    public enum Prono
    {
        Rien, Domicile, Exterieur, Nul
    }
}
